package com.executor;

import com.offbytwo.jenkins.*;
import com.offbytwo.jenkins.model.Job;
import java.net.URI;
import java.util.*;


public class JenkinsTrigger
{
    static String userName;
    static String userPassword;
    static String url;
    static String jobName;
    static String browserValue;
    
    public static JenkinsTrigger getInstance()
    {
        return new JenkinsTrigger();
    }
    
    public JenkinsTrigger triggerJob(String userName,String userPassword,String url,String jobName,String browserValue) throws Exception
    {
        this.userName=userName;
        this.userPassword=userPassword;
        this.url=url;
        this.jobName=jobName;
        this.browserValue=browserValue;
        
        Map<String,String> params= new HashMap<>();
        params.put("Browser", browserValue);
        JenkinsServer jenkins = new JenkinsServer(new URI(url),userName,userPassword); 
        Job myJob= jenkins.getJob(jobName);
        String queue = myJob.build(params, true).getQueueItemUrlPart();
        return this;
    }
}
