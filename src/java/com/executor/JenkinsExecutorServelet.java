package com.executor;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class JenkinsExecutorServelet extends HttpServlet
{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        
        String userName=req.getParameter("user_name");
        String userPassword=req.getParameter("user_password");
        String jenkinsURL=req.getParameter("jenkins_url");
        String jenkinsPort=req.getParameter("jenkins_port");
        String jobName=req.getParameter("Job_Name");
        String browserName=req.getParameter("Browser_Name");
        
        resp.setContentType("text/html");
        PrintWriter out= resp.getWriter();
        
        try
        {
            //http://localhost:8080/
            String jenkinsURLValue = jenkinsURL+":"+jenkinsPort+"/";
            JenkinsTrigger.getInstance().triggerJob(userName, userPassword, jenkinsURLValue, jobName, browserName);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        out.println("<h2>Request has been sent to Jenkins</h2>");
        out.println("<h2>Click on below link to view the progress</h2>");
        String value =jenkinsURL+":"+jenkinsPort+"/job/"+jobName+"/build?delay=0sec";
        out.println("<a href="+value+">Click me</a>");
        
        
    }
    
}
