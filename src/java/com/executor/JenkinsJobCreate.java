package com.executor;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Job;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.net.URI;
import java.util.*;


public class JenkinsJobCreate 
{
    public static void main(String[] args) throws Exception
    {
        JenkinsServer jenkins = new JenkinsServer(new URI("http://localhost:8080/"), "loveen", "loveen");
        Map<String, Job> jobs = jenkins.getJobs();
    
        File xmlFile = new File("web/WEB-INF/jenkins.xml");
        Reader fileReader = new FileReader(xmlFile);
        BufferedReader bufReader = new BufferedReader(fileReader);
        StringBuilder sb = new StringBuilder();
        String line = bufReader.readLine();
        while( line != null)
        {
            sb.append(line).append("\n");
            line = bufReader.readLine();
        }
        String jobXml = sb.toString();
        System.out.println("XML to String using BufferedReader : ");
        System.out.println(jobXml);
    
    
        System.out.println("Calling run method :" + jenkins.getJob("WebApplicationTesting"));
    
    
    
        jenkins.createJob("secondJob", jenkins.getJobXml("WebApplicationTesting"), true);
        //jenkins.getJobXml("WebApplicationTesting")
        //jobXml 
        
        
    }

    
    
}
